﻿namespace MyApp // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var dealer = new Dealer();

            // car1. You have no idea what car you are getting. Dealer sells cars
            // You don't have access to FuelTank or Battery,
            // but you know for sure that every Car has manufacturer
            Console.WriteLine("==========car1===========");
            var car1 = dealer.GetMeBestCarForReason(Reason.AwesomeCarPleaseClimateIsOk);
            Console.WriteLine(car1.Manufacturer);


            // car2. You assume gasoline car, since climate is ok
            // You have access to FuelTankCapacity
            Console.WriteLine("==========car2===========");
            var car2 = (GasolineCar) dealer.GetMeBestCarForReason(Reason.AwesomeCarPleaseClimateIsOk);
            Console.WriteLine(car2.Manufacturer);
            Console.WriteLine(car2.FuelTankCapacity);



            // car3. You assume electric car, since Climate is fucked
            // You have access to BatteryCapacity
            Console.WriteLine("==========car3===========");
            var car3 = (ElectricCar) dealer.GetMeBestCarForReason(Reason.ClimateChangeArrivedWeAreFucked);
            Console.WriteLine(car3.Manufacturer);
            Console.WriteLine(car3.BatteryCapacity);
        }
    }

    abstract class Car
    {
        public string Manufacturer { get; set; }
    }

    class ElectricCar : Car
    {
        public string BatteryCapacity => "This one has 100kW";
    }

    class GasolineCar : Car
    {
        public string FuelTankCapacity => "This one has 55L";
    }

    class Dealer
    {
        public Car GetMeBestCarForReason(Reason reason)
        {
            switch (reason)
            {
                case Reason.ClimateChangeArrivedWeAreFucked:
                    return new ElectricCar() {Manufacturer = "Tesla"};
                case Reason.AwesomeCarPleaseClimateIsOk:
                    return new GasolineCar() {Manufacturer = "BMW"};
                default:
                    throw new ArgumentException("Unsupported climate state. We are either fucked or not");
            }
        }
    }

    public enum Reason
    {
        ClimateChangeArrivedWeAreFucked,
        AwesomeCarPleaseClimateIsOk
    }
}